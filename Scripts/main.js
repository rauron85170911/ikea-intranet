$(document).ready(function(){

    /* Funcionalidad para mostrar/ocultar password en el Login */
    $('#loginForm .login-password').on('click','.icon-eye',function(){
        // console('inicio evento');
        var $contenedorPassword = $(this).closest('.login-password');
        var $tipoElemento = $contenedorPassword.find('input').attr('type');
        console.log($tipoElemento);
        var $tituloMostrar = 'Mostrar Contraseña';
        var $tituloOcultar = 'Ocultar Contraseña';
        var $ojoAbierto = 'M12 17c3.975 0 7.456-3.554 8.706-4.999C19.454 10.558 15.963 7 12 7c-3.975 0-7.456 3.554-8.706 4.999C4.546 13.442 8.037 17 12 17zm0-11c5.523 0 10 6 10 6s-4.477 6-10 6-10-6-10-6 4.477-6 10-6zm0 2a4 4 0 1 1 0 8 4 4 0 0 1 0-8z';
        var $ojoCerrado = 'M19.646 20.354l-16-16 .708-.708 16 16-.708.708zm-3.963-6.792l-5.245-5.245a4 4 0 0 1 5.245 5.245zm-7.366-3.124l5.245 5.245a4 4 0 0 1-5.245-5.245zM12 18c-5.523 0-10-6-10-6s1.592-2.12 4.038-3.841l.714.714c-1.583 1.088-2.816 2.384-3.458 3.126C4.546 13.442 8.037 17 12 17c.844 0 1.663-.167 2.446-.433l.776.776C14.21 17.742 13.129 18 12 18zm0-12c5.523 0 10 6 10 6s-1.592 2.12-4.038 3.841l-.714-.714c1.583-1.088 2.816-2.384 3.458-3.126C19.454 10.558 15.963 7 12 7c-.844 0-1.663.167-2.446.433l-.776-.776C9.79 6.259 10.871 6 12 6z';

        if($tipoElemento == "password") {
            $contenedorPassword.find('input').removeAttr('type');
            $contenedorPassword.find('input').attr('type','text');
            $contenedorPassword.find('title').text($tituloOcultar);
            $contenedorPassword.find('path').removeAttr('d');
            $contenedorPassword.find('path').attr('d',$ojoCerrado);
        }else{
            $contenedorPassword.find('input').removeAttr('type');
            $contenedorPassword.find('input').attr('type','password');
            $contenedorPassword.find('title').text($tituloMostrar);
            $contenedorPassword.find('path').attr('d',$ojoAbierto);
        }
    });

    /* modificar posición de imagen segun tamaño de input password */
    $login = $('body').find('#loginForm');
    if($login.length > 0){
        var $text =$('#loginForm .login-password').find('input');
        if($text.hasClass('input-lg')){
            //console.log('input-lg');
        }else if ($text.hasClass('input-sm')) {
            //console.log('input-sm');
            $('.login-password').find('svg').addClass('sm');
        }else{
            //console.log('input-md default');
            $('.login-password').find('svg').addClass('md');
        }
    }

    /* Modificar los enlaces de tabla por iconos */
    $('#listado-usuarios table tbody td a').each(function(){
        $this = $(this);
        $texto = $this.text();
        //console.log($texto);
        switch ($texto) {
            case "Editar":
                //console.log('es editar');
                $this.html('<img src="/Images/ico_editar.png" alt="Editar"> ');
              break;
            case "Borrar":
                //console.log('es borrar')
                $this.html('<img src="/Images/ico_borrar.png" alt="Eliminar"> ');
              break;
            case "Consultar":
                //console.log('es editar');
                $this.html('<img src="/Images/ico_Listar.svg" alt="Consultar" width="26px" height="26px"> ');
              break;
            default:
            // no se hace nada
            //console.log('no es ninguno');
          }
    });

    /* Funcionalidad para la visualización del ascensor */
        if ($(this).scrollTop() > 100) {
            $('.GoTop').fadeIn();
        } else {
            $('.GoTop').fadeOut();
        }
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
            $('.GoTop').fadeIn();
            } else {
            $('.GoTop').fadeOut();
            }
        });
        /* Funcionalidad para subir con el ascensor */
        $('.GoTop').click(function () {
            $('html, body').animate({ scrollTop: 0 }, 600);
            //return false;
        });
});